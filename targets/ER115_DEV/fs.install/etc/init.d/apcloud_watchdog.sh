#!/bin/sh

echo "Starting apcloud_watchdog..."
[ -f /data/apcloud_watchdog ] && [ -f /data/apcloud ] && [ -f /data/apcloud_config.txt ] && {
	echo "apcloud_watchdog, apcloud and apcloud_config.txt file found under /data"
	/data/apcloud_watchdog /data/ apcloud_config.txt 
	exit 0
}

echo "apcloud_watchdog, apcloud and apcloud_config.txt file not found under /data/, copy from /usr/bin and /usr/config"
cp /usr/bin/apcloud_watchdog /data/apcloud_watchdog
cp /usr/bin/apcloud /data/apcloud
cp /usr/bin/betterspeedtest /data/betterspeedtest
cp /usr/bin/bandwidth /data/bandwidth
cp /usr/config/apcloud_config.txt /data/apcloud_config.txt

chmod 755 /data/apcloud_watchdog
chmod 755 /data/apcloud
chmod 755 /data/betterspeedtest
chmod 755 /data/bandwidth
chmod 755 /data/apcloud_config.txt
/data/apcloud_watchdog /data/ apcloud_config.txt
exit 0
